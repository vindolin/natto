Intended as a debug tool, natto returns a representation of python data structures as pretty HTML tables.
=========================================================================================================
:Author:
    Thomas Schüßler <vindolin@gmail.com>

About
=====
Take a look at http://natto.atomar.de/ to get an idea what the output looks like.

Screenshot
==========
.. image:: http://natto.atomar.de/static/images/screenshot.jpg
   :scale: 50 %
   :alt: screenshot
   :target: http://natto.atomar.de/static/images/screenshot.jpg

Requirements
============
    * python >= 2.5
    * modern webbrowser (IE works but is ugly)

Usage
=====
Usage example::

    from natto import natto
    natto_html = natto(some_data_structure, with_css=True)
....
TODO
====
    lots of things... this is alpha

Changelog
=========

0.1.6 ~ July 25, 2011
---------------------
- new option: blacklist
- use __all__ instead of __dir__ where existent
- option max_h defaults to 20

0.1.5 ~ July 23, 2011
---------------------
- fixed broken setup.py

0.1.4 ~ July 22, 2011
---------------------
- fixed broken javascript

Bitbucket Repo
===========
https://bitbucket.org/vindolin/natto
