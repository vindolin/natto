# -*- coding: utf-8 -*-

from config import DEBUG
import sys

from flask import Flask
from flask import render_template
from flask import make_response, request, app
from natto import natto
from scss import Scss
from time import time

import flask

from collections import *

app = Flask(__name__)

_int   = 3
_float = 3.0
_list  = [1,2,3]
_tuple = (1,2,3)

_recursion = [1,2,3]
_recursion[1] = [1,2,_recursion]

def empty_func():
    return 1

test_data = {
    'class2':dict,
    'listiter':iter(_list),
    'int':_int,
    'float':_float,
    'list':_list,
    'same_list':_list,
    'tuple':_tuple,
    'set':set([1,2,3,3]),
    'dict':{'b':2,'a':1,'c':_tuple},
    'dictiter':iter({'a':1,'b':2}),
#    'other':Honk(),
    'recursion':_recursion,
    'recursion2':_recursion,
    'multiline_str':u"'E's a stiff! Bereft of life, 'e rests in peace! If you hadn't nailed 'im to the perch 'e'd be pushing up the daisies!  \n  'Is metabolic processes are now 'istory! 'E's off the twig! \n 	'E's kicked the bucket,	'e's shuffled off 'is mortal coil, run down the curtain and joined the bleedin' choir invisibile!!  	",
    'func':empty_func,
#    'module':lib,
#    'class':lib.Honk,
    'html':'<table id="t1"><tr><td>meep</td></tr></table>',
    'generator': (x for x in _tuple),
}

@app.route("/")
def index():
    print 'meep'
    import natto as testmodule
    content = ''
#    content += natto(test_data, label='Test Data', magic=False, modules=False, sort_attr=True, html_highlight=True)

#    content += natto(testmodule, max_h=4)
    content += natto(request, label='werkzeug request', magics=False, max_i=4000)
#    content += natto(1, magics=False)
#    content += natto(test_data)
    return render_template('index.jinja2', content=content)

@app.route("/style.css")
def stylesheet():
    if 0:
        css = Scss().compile(open('static/css/style.scss').read())
        # workaround for bug in pyscss
        css = css.replace('url(/static/images/expand.gif)', 'url(data:image/gif;base64,R0lGODlhDgAOAKUfADg4OEZGRldXV2dnZ3V1dX9/f5ycnJ+fn6Ojo6ysrLGxsba2tru7u8DAwMXFxcfHx8rKytPT09fX193d3d7e3uDg4OXl5ejo6Onp6evr6+7u7vHx8fT09Pf39/z8/P7+/v////7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/iwAAAAADgAOAAAGaUDQZEgsDkEYkHLJVGIkn6h0GpVEPNisFhuBdL7fQgH8hTg4aDSBkEY7Gpv4YE6PbxoMjV7A7+s1DAsZg4MBAYSDCwoXjIwAAI2MCgkWlZaXlQkIFZydnpwIDwcUpKWmBw8gBqusrasgQQA7)')
        with open('natto/_css.py', 'w') as f:
            f.write('css="""%s"""' % css)
    else:
        css = open('static/css/style.css').read()

    response = make_response(css)
    response.headers['Content-Type'] = 'text/css; charset=utf-8'
    return response

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=8083,debug=DEBUG)

